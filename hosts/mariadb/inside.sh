#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup "otto" "sudo"
user_setup "ashimokawa" "sudo"

source "base/base.sh"
setup_sshd

apt-get install -y --no-install-recommends mariadb-server

install_file "mariadb" "/etc/mysql/mariadb.conf.d/60-codeberg.cnf"
service mariadb reload

# ╔════════════════════╗
# ║Begin database setup║
# ╚════════════════════╝

source "base/secrets.sh"

function mariadb_create_db_and_user () {
	USER_AND_DB_NAME="${1}"
	ALLOWED_REMOTE_HOST="${2}"

	SECRET_VAR="MARIADB_${USER_AND_DB_NAME^^}"
	secret_get "${SECRET_VAR}"
	USER_PASSWORD="${!SECRET_VAR}"

	mysql -e "CREATE DATABASE IF NOT EXISTS \`${USER_AND_DB_NAME}\`;"
	mysql -e "GRANT ALL PRIVILEGES ON ${USER_AND_DB_NAME}.* TO '${USER_AND_DB_NAME}'@'${ALLOWED_REMOTE_HOST}'
  IDENTIFIED BY '${USER_PASSWORD}';"
}

mariadb_create_db_and_user "gitea_staging" "gitea-staging.lxc.local"
mariadb_create_db_and_user "gitea_production" "gitea-production.lxc.local"
mariadb_create_db_and_user "voting" "ev.lxc.local"
