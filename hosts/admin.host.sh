#!/usr/bin/env bash

set -ex

MEM_HIGH=6
CPU_MAX=6
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = /mnt/ceph-cluster mnt/ceph-cluster none defaults,bind,create=dir 0 0
security.nesting = true
EOF
)
