#!/usr/bin/env bash

set -ex

source "base/users.sh"

# ensure git has id 1001
if [ "$(id -u "git" 2>/dev/null)" == "" ]; then
	useradd --uid 1001 git
	mkdir -p /home/git
	chown git:git /home/git -R
fi

source "base/base.sh"
setup_sshd

user_grant "root" "otto"
user_grant "root" "ashimokawa"
user_grant "root" "hw"
systemctl reload sshd

apt-get install -y --no-install-recommends git mariadb-client rsync redis-server

## redis setup
line_in_file /etc/redis/redis.conf "maxmemory 1gb"
line_in_file /etc/redis/redis.conf "maxmemory-policy allkeys-lru"
## done

mkdir -p /data/git
# mkdir -p /mnt/ceph-cluster
chown git:git /data/git
chown git:git /mnt/ceph-cluster


# custom renderers
apt-get install -y --no-install-recommends pandoc asciidoctor
line_in_file /etc/sudoers "git ALL=(nobody) NOPASSWD: /usr/bin/pandoc"
line_in_file /etc/sudoers "git ALL=(nobody) NOPASSWD: /usr/bin/asciidoctor"
