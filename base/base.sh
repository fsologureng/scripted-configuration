#!/usr/bin/env bash

set -ex

function setup_sshd () {
	apt-get install -y openssh-server
	cat <<EOF > /etc/ssh/sshd_config
Port 22

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

X11Forwarding yes
PrintMotd no

AuthorizedKeysFile .ssh/authorized_keys
PermitRootLogin no

PubkeyAuthentication yes
PasswordAuthentication no
ChallengeResponseAuthentication no
KerberosAuthentication no
GSSAPIAuthentication no
UsePAM yes

# Only accept hardware-backed keys (SK)
PubkeyAcceptedKeyTypes sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,sk-ecdsa-sha2-nistp256@openssh.com,sk-ecdsa-sha2-nistp256-cert-v01@openssh.com

# Disabled, as it causes errors if the locale isn't installed
#AcceptEnv LANG LANGUAGE LC_*

Subsystem	sftp	/usr/lib/openssh/sftp-server
EOF
	chmod 'u=rw,g=r,o=r' /etc/ssh/sshd_config
	systemctl restart sshd
}

function enable_backports () {
	BACKPORT_APTSOURCE_LINE="deb http://deb.debian.org/debian bullseye-backports main"
	if [ "$(grep "${BACKPORT_APTSOURCE_LINE}" /etc/apt/sources.list.d/backports.list)" == "" ]; then
		echo "${BACKPORT_APTSOURCE_LINE}" >> /etc/apt/sources.list.d/backports.list
	fi
	apt-get update
}

# install_file is used to copy a file from the config to the host system including the creation if a backup file
# usage: install_file "host" "/absolute/path/to/file"
# example: install_file "mariadb" "/etc/mysql/my.cnf" ;# (copies hosts/mariadb/etc/mysql/my.cnf)
# Files are placed into hosts/<host>/<absolute path>. The host is used to allow potentially sharing files between hosts and reusing them.
function install_file () {
	HOST="${1}"
	FILEPATH="${2}"
	
	DIFF="$(diff --new-file "./hosts/${HOST}${FILEPATH}" "${FILEPATH}" || cp -f "${FILEPATH}" "./hosts/${HOST}${FILEPATH}.$(date +"%Y-%m-%d").backup" || true)"
	echo "${DIFF}"

	cp -f "./hosts/${HOST}${FILEPATH}" "${FILEPATH}"
}

function line_in_file () {
	FILENAME="${1}"
	LINE="${2}"

	if ! grep --fixed-strings --line-regexp "${LINE}" "${FILENAME}" ; then
		echo "${LINE}" >> "${FILENAME}"
	fi
}

function confirm {
	# $1: Confirmation dialog
	# return:
	#	0 = true
	#	1 = false
	read -p "$1 [y/N] " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		return 0
	else
		return 1
	fi
}

